# README #
Very simple rainbow effect for a strip of Adafruit dotstar LEDs on Feather M0 Bluefruit LE

### Parts used: ###
* https://www.adafruit.com/product/2995
* https://www.adafruit.com/product/2343

### Setup ###

#### Set up Arduino environment for Feather M0 Bluefruit LE ####
 https://learn.adafruit.com/adafruit-feather-m0-bluefruit-le/setup and the next step:
 https://learn.adafruit.com/adafruit-feather-m0-bluefruit-le/using-with-arduino-ide
#### Install library for dotstar LEDs ####
 https://learn.adafruit.com/adafruit-dotstar-leds/arduino-library-installation
#### Build and run ####
 Open `dotstar-26-wheel.ino`, compile and upload to the board.