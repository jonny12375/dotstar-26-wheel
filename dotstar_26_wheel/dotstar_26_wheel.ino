#include <Adafruit_DotStar.h>
#include <SPI.h>

#define NUMPIXELS 26 // Number of LEDs in strip

#define DATAPIN    13
#define CLOCKPIN   11
Adafruit_DotStar strip = Adafruit_DotStar(
        NUMPIXELS, DATAPIN, CLOCKPIN, DOTSTAR_BRG);

void setup() {
    strip.begin(); // Initialize pins for output
    strip.show();  // Turn all LEDs off ASAP
}

void loop() {
    // Loop from 0 to 255 (roll over 8-bit unsigned int)
    for (uint8_t i = 0;; i++) {
        for (uint8_t led = 0; led < NUMPIXELS; led++) {
            uint32_t colour = Wheel(i + led * (256 / NUMPIXELS));
            strip.setPixelColor(led, colour);
        }
        strip.show();
        delay(5);
    }
}

static uint32_t Wheel(byte WheelPos) {
    WheelPos = 255 - WheelPos;
    if (WheelPos < 85) {
        return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
    }
    if (WheelPos < 170) {
        WheelPos -= 85;
        return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
    }
    WheelPos -= 170;
    return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}
